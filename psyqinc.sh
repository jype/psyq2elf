#!/bin/sh

set -e

usage() {
	printf 'usage: %s <Psy-Q include dir> <output include dir>\n' "$0" 1>&2
}

tolower() {
	tr '[:upper:]' '[:lower:]'
}

PSYQ_DIR="$1"
OUT_DIR="$2"

[ ! -d "$PSYQ_DIR" ] && { usage; exit 1; }
[ -z "$OUT_DIR" ] && { usage; exit 1; }

mkdir -p "$OUT_DIR"

for psyq_inc in "$PSYQ_DIR"/*.H "$PSYQ_DIR"/SYS/*.H; do
	[ ! -f "$psyq_inc" ] && continue

	inc="$(printf '%s' "${psyq_inc#${PSYQ_DIR}}" | tolower)"
	out_inc="$OUT_DIR/${inc#/}"

	printf '%s -> %s\n' "$psyq_inc" "$out_inc"

	mkdir -p "$(dirname "$out_inc")"
	cp "$psyq_inc" "$out_inc"
done

/* See LICENSE file for copyright and license details. */

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "psyq_obj.h"
#include "slist.h"
#include "utils.h"

#define PSYQ_LNK_SIGNATURE	"LNK"

typedef enum psyq_state_type {
	psyq_state_eof = 0,
	psyq_state_code = 2,
	psyq_state_switch = 6,
	psyq_state_bss_alloc = 8,
	psyq_state_patch = 10,
	psyq_state_def = 12,
	psyq_state_ref = 14,
	psyq_state_section = 16,
	psyq_state_local = 18,
	psyq_state_file = 28,
	psyq_state_processor = 46,
	psyq_state_bss = 48,
} psyq_state_type;

typedef enum psyq_patch_op {
	psyq_patch_op_add = 44,
	psyq_patch_op_sub = 46,
	psyq_patch_op_div = 50,
	psyq_patch_op_exc = 54,
} psyq_patch_op;

typedef struct psyq_reader {
	struct psyq_link *cur_link;
	struct psyq_section *cur_section;
} psyq_reader;

typedef struct psyq_patch_node {
	struct psyq_patch_node *left;
	struct psyq_patch_node *right;
	uint32_t value;
	uint16_t symbol;
	psyq_patch_type type;
	psyq_patch_op op;
} psyq_patch_node;

static psyq_section *
find_section(slist_psyq_section *sections, uint16_t index)
{
	psyq_section *section;
	size_t i;

	for (i = 0; i < slist_psyq_section_size(sections); ++i) {
		section = slist_psyq_section_get(sections, i);
		if (section->symbol == index)
			return section;
	}

	return NULL;
}

static void
read_state_code(FILE *file, psyq_reader *reader)
{
	psyq_section *section;
	uint8_t *code;
	uint16_t size;

	section = reader->cur_section;
	assert((section != NULL) && !section->bss_alloc_size);

	READ_FIELD(file, size);
	assert(size);

	code = xrealloc(section->code, section->code_size + size);
	assert(code != NULL);

	assert(fread(code + section->code_size, size, 1, file) == 1);

	section->code = code;
	section->code_size += size;
}

static void
read_state_switch(FILE *file, psyq_reader *reader)
{
	psyq_link *link = reader->cur_link;
	psyq_section *section;
	uint16_t index;

	READ_FIELD(file, index);

	section = find_section(&link->sections, index);
	assert(section != NULL);

	reader->cur_section = section;
}

static void
read_state_bss_alloc(FILE *file, psyq_reader *reader)
{
	psyq_section *section;
	uint32_t size;

	section = reader->cur_section;
	assert((section != NULL) && (section->code == NULL) &&
	       !section->code_size && !section->bss_alloc_size);

	READ_FIELD(file, size);
	assert(size);

	section->bss_alloc_size = size;
	section->real_bss_size += size;
}

static void
read_patch_node(FILE *file, psyq_patch_node *node)
{
	uint8_t state;

	READ_FIELD(file, state);

	if (state <= 24) {
		switch (state) {
		case 0:
		case psyq_patch_value:
			READ_FIELD(file, node->value);
			node->type = psyq_patch_value;
			break;
		case psyq_patch_ref:
			READ_FIELD(file, node->symbol);
			node->type = psyq_patch_ref;
			break;
		case psyq_patch_section_base:
			READ_FIELD(file, node->symbol);
			node->type = psyq_patch_section_base;
			break;
		case psyq_patch_section_start:
			READ_FIELD(file, node->symbol);
			node->type = psyq_patch_section_start;
			break;
		case psyq_patch_section_end:
			READ_FIELD(file, node->symbol);
			node->type = psyq_patch_section_end;
			break;
		default:
			assert(0);
			break;
		}
	} else {
		node->type = psyq_patch_expr;

		switch (state) {
		case psyq_patch_op_add:
		case psyq_patch_op_sub:
		case psyq_patch_op_div:
		case psyq_patch_op_exc:
			break;
		default:
			assert(0);
			break;
		}

		node->op = state;

		node->left = xcalloc(1, sizeof(*node->left));
		node->right = xcalloc(1, sizeof(*node->right));

		read_patch_node(file, node->left);
		read_patch_node(file, node->right);
	}
}

static void
read_state_patch(FILE *file, psyq_reader *reader)
{
	psyq_section *section;
	psyq_patch *patch;
	psyq_patch_node *node;
	psyq_patch_node *tmp;
	uint8_t type;

	section = reader->cur_section;
	assert(section != NULL);

	patch = slist_psyq_patch_add(&section->patches);
	assert(patch != NULL);

	READ_FIELD(file, type);

	switch (type) {
	case psyq_reloc_word_literal:
	case psyq_reloc_function_call:
	case psyq_reloc_upper_immediate:
	case psyq_reloc_lower_immediate:
		patch->reloc_type = type;
		break;
	default:
		assert(0);
		break;
	}

	READ_FIELD(file, patch->offset);

	node = xcalloc(1, sizeof(*node));

	read_patch_node(file, node);

	if ((node->type == psyq_patch_expr) &&
	    (node->left->type == psyq_patch_value)) {
		if (node->right->type == psyq_patch_section_base) {
			tmp = node->left;
			node->left = node->right;
			node->right = tmp;
		} else if (node->right->type != psyq_patch_ref) {
			node = node->right;
		}
	}

	switch (node->type) {
	case psyq_patch_expr:
		switch (node->left->type) {
		case psyq_patch_section_base:
			assert((node->right->type == psyq_patch_value) &&
			       (node->op == psyq_patch_op_add));

			/* Create symbol in <section> at <value> */
			patch->type = psyq_patch_section_base;
			patch->symbol = node->left->symbol;
			patch->value = node->right->value;
			break;
		case psyq_patch_section_start:
			assert((node->right->type ==
				psyq_patch_section_end) &&
			       (node->left->symbol == node->right->symbol) &&
			       (node->op == psyq_patch_op_sub));

			/* Create symbol __<section>_size__ */
			patch->type = psyq_patch_section_size;
			patch->symbol = node->left->symbol;
			break;
		case psyq_patch_value:
			assert((node->right->type == psyq_patch_ref) &&
			       ((node->op == psyq_patch_op_add) ||
				(node->op == psyq_patch_op_sub)));

			/* Reference <symbol> ignoring value */
			patch->type = psyq_patch_ref;
			patch->symbol = node->right->symbol;
			break;
		default:
			assert(0);
			break;
		}
		break;
	case psyq_patch_ref:
		/* Reference <symbol> */
		patch->type = psyq_patch_ref;
		patch->symbol = node->symbol;
		break;
	case psyq_patch_section_base:
		/* Reference <section> symbol */
		patch->type = psyq_patch_section_base;
		patch->symbol = node->symbol;
		break;
	case psyq_patch_section_start:
		/* Create symbol __<section>_start__ */
		patch->type = psyq_patch_section_start;
		patch->symbol = node->symbol;
		break;
	case psyq_patch_section_end:
		/* Create symbol __<section>_end__ */
		patch->type = psyq_patch_section_end;
		patch->symbol = node->symbol;
		break;
	default:
		assert(0);
		break;
	}
}

static void
read_state_def(FILE *file, psyq_reader *reader)
{
	psyq_sym *sym;

	sym = slist_psyq_sym_add(&reader->cur_link->syms);
	assert(sym != NULL);

	sym->type = psyq_symbol_internal;

	READ_FIELD(file, sym->symbol);
	READ_FIELD(file, sym->section);
	READ_FIELD(file, sym->offset);

	READ_STRING(file, sym->name);
}

static void
read_state_ref(FILE *file, psyq_reader *reader)
{
	psyq_sym *sym;

	sym = slist_psyq_sym_add(&reader->cur_link->syms);
	assert(sym != NULL);

	sym->type = psyq_symbol_external;

	READ_FIELD(file, sym->symbol);

	READ_STRING(file, sym->name);
}

static void
read_state_section(FILE *file, psyq_reader *reader)
{
	psyq_section *section;

	section = slist_psyq_section_add(&reader->cur_link->sections);
	assert(section != NULL);

	READ_FIELD(file, section->symbol);
	READ_FIELD(file, section->group);
	READ_FIELD(file, section->alignment);

	READ_STRING(file, section->name);
}

static void
read_state_local(FILE *file, psyq_reader *reader)
{
	psyq_sym *sym;

	sym = slist_psyq_sym_add(&reader->cur_link->syms);
	assert(sym != NULL);

	sym->type = psyq_symbol_local;

	READ_FIELD(file, sym->section);
	READ_FIELD(file, sym->offset);

	READ_STRING(file, sym->name);
}

static void
read_state_file(FILE *file, psyq_reader *reader)
{
	uint16_t symbol;
	char str[PSYQ_NAME_MAX];

	(void)reader;

	READ_FIELD(file, symbol);

	READ_STRING(file, str);
}

static void
read_state_processor(FILE *file, psyq_reader *reader)
{
	uint8_t type;

	(void)reader;

	READ_FIELD(file, type);
}

static void
read_state_bss(FILE *file, psyq_reader *reader)
{
	psyq_link *link = reader->cur_link;
	psyq_section *section;
	psyq_sym *sym;
	uint32_t alignment;

	sym = slist_psyq_sym_add(&reader->cur_link->syms);
	assert(sym != NULL);

	sym->type = psyq_symbol_bss;

	READ_FIELD(file, sym->symbol);
	READ_FIELD(file, sym->section);
	READ_FIELD(file, sym->size);

	READ_STRING(file, sym->name);

	section = find_section(&link->sections, sym->section);
	assert((section != NULL) && (section->code == NULL) &&
	       !section->code_size);

	alignment = MIN(sym->size, section->alignment) - 1;
	section->real_bss_size += alignment;
	section->real_bss_size &= ~alignment;

	section->real_bss_size += sym->size;
}

typedef struct state_handler {
	enum psyq_state_type state;
	void (*handle)(FILE *, psyq_reader *);
} state_handler;

static state_handler state_handlers[] = {
	{ psyq_state_code,	read_state_code		},
	{ psyq_state_switch,	read_state_switch	},
	{ psyq_state_bss_alloc,	read_state_bss_alloc	},
	{ psyq_state_patch,	read_state_patch	},
	{ psyq_state_def,	read_state_def		},
	{ psyq_state_ref,	read_state_ref		},
	{ psyq_state_section,	read_state_section	},
	{ psyq_state_local,	read_state_local	},
	{ psyq_state_file,	read_state_file		},
	{ psyq_state_processor,	read_state_processor	},
	{ psyq_state_bss,	read_state_bss		},
};

static state_handler *
find_state_handler(uint8_t state)
{
	size_t i;

	for (i = 0; i < ARRAY_SIZE(state_handlers); ++i) {
		if (state_handlers[i].state == state)
			return &state_handlers[i];
	}

	return NULL;
}

static void
apply_patches_mask(psyq_link *link)
{
	psyq_section *section;
	psyq_patch *patch;
	uint32_t *offset;
	size_t patches_size;
	uint32_t mask;
	uint32_t pos;
	size_t i;
	size_t j;

	for (i = 0; i < slist_psyq_section_size(&link->sections); ++i) {
		section = slist_psyq_section_get(&link->sections, i);

		if (section->code == NULL)
			continue;

		patches_size = slist_psyq_patch_size(&section->patches);
		for (j = 0; j < patches_size; ++j) {
			patch = slist_psyq_patch_get(&section->patches, j);

			if (patch->type != psyq_patch_section_base)
				continue;

			pos = patch->offset + 4;
			assert(pos <= section->code_size);
			assert(!(patch->offset % 4));

			switch (patch->reloc_type) {
			case psyq_reloc_word_literal:
				mask = 0;
				break;
			case psyq_reloc_function_call:
				mask = 0xfc000000;
				break;
			case psyq_reloc_upper_immediate:
			case psyq_reloc_lower_immediate:
				mask = 0xffff0000;
				break;
			default:
				assert(0);
				break;
			}

			offset = (uint32_t *)(section->code + patch->offset);
			*offset &= mask;
		}
	}
}

int
psyq_parse_link(FILE *file, psyq_link *link)
{
	psyq_reader reader;
	state_handler *handler;
	uint8_t signature[3];
	uint8_t version;
	uint8_t state;

	assert(file != NULL);
	assert(link != NULL);

	memset(&reader, 0, sizeof(reader));
	memset(link, 0, sizeof(*link));

	reader.cur_link = link;
	reader.cur_section = NULL;

	READ_FIELD(file, signature);
	assert(!memcmp(signature, PSYQ_LNK_SIGNATURE,
		       strlen(PSYQ_LNK_SIGNATURE)));

	READ_FIELD(file, version);
	assert(version == 2);

	while (!feof(file) && !ferror(file)) {
		READ_FIELD(file, state);

		if (state == psyq_state_eof)
			break;

		handler = find_state_handler(state);
		assert(handler != NULL);

		handler->handle(file, &reader);
	}

	if ((state != psyq_state_eof) || ferror(file))
		return 1;

	apply_patches_mask(link);

	return 0;
}

SLIST_DEF(psyq_sym);
SLIST_DEF(psyq_patch);
SLIST_DEF(psyq_section);

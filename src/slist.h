/* See LICENSE file for copyright and license details. */

#ifndef PSYQ2ELF_SLIST_H
#define PSYQ2ELF_SLIST_H

#include <stdlib.h>

#define SLIST_DECL(type)					\
typedef struct slist_##type {					\
	type *data;						\
	size_t size;						\
	size_t capacity;					\
} slist_##type;							\
								\
size_t slist_##type##_size(const slist_##type *l);		\
type *slist_##type##_get(slist_##type *l, size_t i);		\
type *slist_##type##_add(slist_##type *l);

#define SLIST_DEF(type)						\
size_t								\
slist_##type##_size(const slist_##type *l)			\
{								\
	return l->size;						\
}								\
								\
type *								\
slist_##type##_get(slist_##type *l, size_t i)			\
{								\
	return &l->data[i];					\
}								\
								\
type *								\
slist_##type##_add(slist_##type *l)				\
{								\
	type *data = l->data;					\
	size_t capacity = l->capacity;				\
	size_t size;						\
								\
	if (l->size == capacity) {				\
		if (!capacity)					\
			capacity = 1;				\
		else						\
			capacity *= 2;				\
								\
		size = capacity * sizeof(type);			\
		data = realloc(data, size);			\
		if (data == NULL)				\
			return NULL;				\
								\
		l->data = data;					\
		l->capacity = capacity;				\
	}							\
								\
	data = &l->data[l->size];				\
	memset(data, 0, sizeof(type));				\
	++l->size;						\
								\
	return data;						\
}

#endif

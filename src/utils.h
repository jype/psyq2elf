/* See LICENSE file for copyright and license details. */

#ifndef PSYQ2ELF_UTILS_H
#define PSYQ2ELF_UTILS_H

#include <assert.h>
#include <stddef.h>
#include <stdio.h>

#define MIN(a, b)	((a) < (b) ? (a) : (b))
#define MAX(a, b)	((a) > (b) ? (a) : (b))

#define ARRAY_SIZE(a)	(sizeof(a) / sizeof(*(a)))

#define READ_FIELD(file, field)	\
	assert(fread(&(field), sizeof(field), 1, file) == 1);

#define READ_STRING(file, str)				\
	do {						\
		uint8_t size;				\
		READ_FIELD(file, size);			\
		assert(size < sizeof(str));		\
		assert(fread(str, size, 1, file) == 1);	\
		str[size] = '\0';			\
	} while(0);

void *xcalloc(size_t nmemb, size_t size);
void *xrealloc(void *ptr, size_t size);

#endif

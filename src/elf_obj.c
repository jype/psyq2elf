/* See LICENSE file for copyright and license details. */

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <elf.h>

#include "elf_obj.h"
#include "psyq_obj.h"
#include "utils.h"

static elf_section *
find_section(slist_elf_section *sections, uint16_t symbol, uint32_t *index)
{
	elf_section *esection;
	psyq_section *psection;
	size_t i;

	for (i = 0; i < slist_elf_section_size(sections); ++i) {
		esection = slist_elf_section_get(sections, i);
		psection = esection->psection;

		if ((psection != NULL) && (psection->symbol == symbol)) {
			if (index != NULL)
				*index = i;
			return esection;
		}
	}

	return NULL;
}

static elf_sym *
find_sym(slist_elf_sym *syms, uint16_t symbol, uint32_t *index)
{
	elf_sym *esym;
	psyq_sym *psym;
	size_t i;

	for (i = 0; i < slist_elf_sym_size(syms); ++i) {
		esym = slist_elf_sym_get(syms, i);
		psym = esym->psym;
		if ((psym != NULL) && (psym->symbol == symbol)) {
			if (index != NULL)
				*index = i;
			return esym;
		}
	}

	return NULL;
}

static int
find_sym_name(slist_elf_sym *syms, const char *name, uint16_t *index)
{
	elf_sym *esym;
	size_t i;

	for (i = 0; i < slist_elf_sym_size(syms); ++i) {
		esym = slist_elf_sym_get(syms, i);
		if ((esym != NULL) && (esym->name != NULL) &&
		    !strcmp(esym->name, name)) {
			*index = i;
			return 0;
		}
	}

	return 1;
}

static void
add_section_data(elf_section *esection, const void *data, size_t size)
{
	esection->data = xrealloc(esection->data, esection->size + size);
	memcpy(esection->data + esection->size, data, size);

	esection->size += size;
}

static uint16_t
add_external_sym(elf_file *efile, const char *name)
{
	elf_sym *esym;
	char *str;

	esym = slist_elf_sym_add(&efile->syms);
	assert(esym != NULL);

	str = xcalloc(1, strlen(name) + 1);
	strcat(str, name);

	esym->name = str;
	esym->sym.st_info = ELF32_ST_INFO(STB_GLOBAL, STT_NOTYPE);

	return slist_elf_sym_size(&efile->syms) - 1;
}

static uint16_t
find_section_name(elf_file *efile, uint16_t section, const char *postfix)
{
	char buf[PSYQ_NAME_MAX] = { 0 };
	const char *name = NULL;
	elf_sym *esym;
	uint16_t index;
	size_t len;
	size_t i;

	for (i = 0; i < slist_elf_sym_size(&efile->syms); ++i) {
		esym = slist_elf_sym_get(&efile->syms, i);
		if ((esym->psection != NULL) &&
		    (esym->psection->symbol == section)) {
			name = esym->psection->name;
			break;
		}
	}

	assert(name != NULL);
	assert((name[0] == '.') && (strchr(&name[1], '.') == NULL));

	++name;

	strcat(buf, "__");

	len = strlen(buf) + strlen(name) + strlen(postfix);
	assert(len < sizeof(buf));

	strcat(buf, name);
	strcat(buf, postfix);

	if (!find_sym_name(&efile->syms, buf, &index))
		return index;

	if (find_sym_name(&efile->syms, buf, &index))
		index = add_external_sym(efile, buf);

	return index;
}

static uint16_t
add_sym_offset(elf_file *efile, uint16_t section, uint32_t offset)
{
	unsigned char info = ELF32_ST_INFO(STB_LOCAL, STT_NOTYPE);
	elf_sym *esym;
	size_t i;

	for (i = 0; i < slist_elf_sym_size(&efile->syms); ++i) {
		esym = slist_elf_sym_get(&efile->syms, i);
		if (esym->sym.st_value == offset &&
		    esym->sym.st_info == info &&
		    esym->sym.st_shndx == section)
			return i;
	}

	esym = slist_elf_sym_add(&efile->syms);
	assert(esym != NULL);

	esym->sym.st_value = offset;
	esym->sym.st_info = info;
	esym->sym.st_shndx = section;

	return slist_elf_sym_size(&efile->syms) - 1;
}

typedef struct shdr_opts {
	const char *name;
	uint32_t type;
	uint32_t flags;
} shdr_opts;

#define SECT(_name, _type, _flags)	\
	{				\
		.name = _name,		\
		.type = _type,		\
		.flags = _flags,	\
	}

static const shdr_opts sh_opts[] = {
	SECT(".bss",	SHT_NOBITS, SHF_WRITE | SHF_ALLOC),
	SECT(".ctors",	SHT_PROGBITS, SHF_WRITE | SHF_ALLOC),
	SECT(".data",	SHT_PROGBITS, SHF_WRITE | SHF_ALLOC),
	SECT(".dtors",	SHT_PROGBITS, SHF_WRITE | SHF_ALLOC),
	SECT(".text",	SHT_PROGBITS, SHF_ALLOC | SHF_EXECINSTR),
	SECT(".rdata",	SHT_PROGBITS, SHF_ALLOC),
	SECT(".sbss",	SHT_NOBITS, SHF_WRITE | SHF_ALLOC | SHF_MIPS_GPREL),
	SECT(".sdata",	SHT_PROGBITS, SHF_WRITE | SHF_ALLOC | SHF_MIPS_GPREL),
};

#undef SECT

static void
add_section(elf_file *efile, psyq_section *psection)
{
	elf_section *esection;
	elf_sym *esym;
	size_t i;

	for (i = 0; i < ARRAY_SIZE(sh_opts); ++i) {
		if (!strcmp(sh_opts[i].name, psection->name))
			break;
	}

	assert(i < ARRAY_SIZE(sh_opts));

	esection = slist_elf_section_add(&efile->sections);
	assert(esection != NULL);

	esection->name = psection->name;
	esection->psection = psection;

	esection->shdr.sh_type = sh_opts[i].type;
	esection->shdr.sh_flags = sh_opts[i].flags;

	if (psection->code != NULL && psection->code_size) {
		esection->data = psection->code;
		esection->size = psection->code_size;
		esection->shdr.sh_size = psection->code_size;
	} else if (psection->code == NULL && psection->real_bss_size) {
		esection->shdr.sh_size = psection->real_bss_size;
	}

	esection->shdr.sh_addralign = 8;

	/* Add section symbol */
	esym = slist_elf_sym_add(&efile->syms);
	assert(esym != NULL);

	esym->name = psection->name;
	esym->psection = psection;

	esym->sym.st_info = ELF32_ST_INFO(STB_LOCAL, STT_SECTION);
	esym->sym.st_shndx = slist_elf_section_size(&efile->sections) - 1;

	esection->sym_index = slist_elf_sym_size(&efile->syms) - 1;
}

static void
add_sym(elf_file *efile, psyq_sym *psym)
{
	elf_section *esection;
	psyq_section *psection;
	elf_sym *esym;
	uint32_t index;
	uint32_t size;
	uint32_t alignment;

	esym = slist_elf_sym_add(&efile->syms);
	assert(esym != NULL);

	esym->name = psym->name;
	esym->psym = psym;

	esection = find_section(&efile->sections, psym->section, &index);

	switch (psym->type) {
	case psyq_symbol_internal:
		assert(esection != NULL);
		esym->sym.st_value = psym->offset;
		esym->sym.st_info = ELF32_ST_INFO(STB_GLOBAL, STT_NOTYPE);
		esym->sym.st_shndx = index;
		break;
	case psyq_symbol_external:
		esym->sym.st_info = ELF32_ST_INFO(STB_GLOBAL, STT_NOTYPE);
		break;
	case psyq_symbol_local:
		assert(esection != NULL);
		esym->sym.st_value = psym->offset;
		esym->sym.st_info = ELF32_ST_INFO(STB_LOCAL, STT_NOTYPE);
		esym->sym.st_shndx = index;
		break;
	case psyq_symbol_bss:
		assert(esection != NULL);
		psection = esection->psection;

		alignment = MIN(psym->size, psection->alignment) - 1;
		esection->bss_offset += alignment;
		esection->bss_offset &= ~alignment;

		size = psection->bss_alloc_size + esection->bss_offset;

		esym->sym.st_value = size;
		esym->sym.st_size = psym->size;
		esym->sym.st_info = ELF32_ST_INFO(STB_GLOBAL, STT_NOTYPE);
		esym->sym.st_shndx = index;

		esection->bss_offset += psym->size;
		break;
	default:
		assert(0);
		break;
	}
}

static void
add_relocs(elf_file *efile, psyq_section *psection)
{
	elf_section *relocs;
	elf_section *esection;
	psyq_patch *patch;
	Elf32_Rel rel;
	char *name;
	uint32_t value;
	uint32_t index;
	uint8_t type;
	size_t i;

	relocs = slist_elf_section_add(&efile->sections);
	assert(relocs != NULL);

	for (i = 0; i < slist_psyq_patch_size(&psection->patches); ++i) {
		patch = slist_psyq_patch_get(&psection->patches, i);

		memset(&rel, 0, sizeof(rel));
		rel.r_offset = patch->offset;

		switch (patch->reloc_type) {
		case psyq_reloc_word_literal:
			type = R_MIPS_32;
			break;
		case psyq_reloc_function_call:
			type = R_MIPS_26;
			break;
		case psyq_reloc_upper_immediate:
			type = R_MIPS_HI16;
			break;
		case psyq_reloc_lower_immediate:
			type = R_MIPS_LO16;
			break;
		default:
			assert(0);
			break;
		}

		switch (patch->type) {
		case psyq_patch_ref:
			assert(find_sym(&efile->syms,
					patch->symbol,
					&index) != NULL);
			break;
		case psyq_patch_section_base:
			esection = find_section(&efile->sections,
						patch->symbol,
						&index);
			assert(esection != NULL);
			value = patch->value;
			if (!value) {
				index = esection->sym_index;
			} else {
				index = add_sym_offset(efile, index, value);
			}
			break;
		case psyq_patch_section_start:
			index = find_section_name(efile,
						  patch->symbol,
						  "_start__");
			break;
		case psyq_patch_section_end:
			index = find_section_name(efile,
						  patch->symbol,
						  "_end__");
			break;
		case psyq_patch_section_size:
			index = find_section_name(efile,
						  patch->symbol,
						  "_size__");
			break;
		default:
			assert(0);
			break;
		}

		rel.r_info = ELF32_R_INFO(index, type);

		add_section_data(relocs, &rel, sizeof(rel));
	}

	relocs->name = ".rel";
	name = xcalloc(1, strlen(relocs->name) + strlen(psection->name) + 1);
	strcat(name, relocs->name);
	strcat(name, psection->name);

	relocs->name = name;
	relocs->shdr.sh_type = SHT_REL;
	relocs->shdr.sh_flags = SHF_INFO_LINK;
	relocs->shdr.sh_size = relocs->size;

	assert(find_section(&efile->sections,
			    psection->symbol,
			    &index) != NULL);

	relocs->shdr.sh_info = index;
	relocs->shdr.sh_addralign = 4;
	relocs->shdr.sh_entsize = sizeof(Elf32_Rel);
}

static void
add_symtab(elf_file *efile)
{
	elf_section *symtab;
	elf_sym *esym;
	uint32_t strtab_size = 1;
	uint32_t last_local = 0;
	size_t i;

	symtab = slist_elf_section_add(&efile->sections);
	assert(symtab != NULL);

	for (i = 0; i < slist_elf_sym_size(&efile->syms); ++i) {
		esym = slist_elf_sym_get(&efile->syms, i);
		if (esym->name != NULL) {
			esym->sym.st_name = strtab_size;
			strtab_size += strlen(esym->name) + 1;
		}

		if (ELF32_ST_BIND(esym->sym.st_info) == STB_LOCAL)
			last_local = MAX(esym->sym.st_shndx, last_local);

		add_section_data(symtab, &esym->sym, sizeof(esym->sym));
	}

	symtab->name = ".symtab";
	symtab->shdr.sh_type = SHT_SYMTAB;
	symtab->shdr.sh_size = symtab->size;
	symtab->shdr.sh_info = last_local + 1;
	symtab->shdr.sh_addralign = 1;
	symtab->shdr.sh_entsize = sizeof(Elf32_Sym);
}

static void
add_strtab(elf_file *efile)
{
	elf_section *strtab;
	elf_sym *esym;
	size_t i = 0;

	strtab = slist_elf_section_add(&efile->sections);
	assert(strtab != NULL);

	add_section_data(strtab, &i, 1);

	for (; i < slist_elf_sym_size(&efile->syms); ++i) {
		esym = slist_elf_sym_get(&efile->syms, i);
		if (esym->name == NULL)
			continue;

		add_section_data(strtab, esym->name, strlen(esym->name) + 1);

		esym->sym.st_name = strtab->size;
	}

	strtab->name = ".strtab";
	strtab->shdr.sh_type = SHT_STRTAB;
	strtab->shdr.sh_size = strtab->size;
	strtab->shdr.sh_addralign = 1;
}

static void
add_shstrtab(elf_file *efile)
{
	elf_section *shstrtab, *esection;
	size_t i = 0;

	shstrtab = slist_elf_section_add(&efile->sections);
	assert(shstrtab != NULL);

	shstrtab->name = ".shstrtab";

	add_section_data(shstrtab, &i, 1);

	for (; i < slist_elf_section_size(&efile->sections); ++i) {
		esection = slist_elf_section_get(&efile->sections, i);
		if (esection->name == NULL)
			continue;

		esection->shdr.sh_name = shstrtab->size;

		add_section_data(shstrtab, esection->name,
				 strlen(esection->name) + 1);
	}

	shstrtab->shdr.sh_type = SHT_STRTAB;
	shstrtab->shdr.sh_size = shstrtab->size;
	shstrtab->shdr.sh_addralign = 1;
}

static void
psyq_to_elf(psyq_link *link, elf_file *efile)
{
	elf_section *esection;
	psyq_section *psection;
	psyq_sym *psym;
	uint32_t nsections;
	uint32_t i;

	memset(efile, 0, sizeof(*efile));

	/* Add null section header */
	assert(slist_elf_section_add(&efile->sections) != NULL);

	/* Add null nymbol */
	assert(slist_elf_sym_add(&efile->syms) != NULL);

	for (i = 0; i < slist_psyq_section_size(&link->sections); ++i) {
		psection = slist_psyq_section_get(&link->sections, i);
		add_section(efile, psection);
	}

	for (i = 0; i < slist_psyq_sym_size(&link->syms); ++i) {
		psym = slist_psyq_sym_get(&link->syms, i);
		add_sym(efile, psym);
	}

	for (i = 0; i < slist_psyq_section_size(&link->sections); ++i) {
		psection = slist_psyq_section_get(&link->sections, i);
		if (!slist_psyq_patch_size(&psection->patches))
			continue;

		add_relocs(efile, psection);
	}

	nsections = slist_elf_section_size(&efile->sections);
	for (i = 0; i < nsections; ++i) {
		esection = slist_elf_section_get(&efile->sections, i);
		if (esection->shdr.sh_type == SHT_REL)
			esection->shdr.sh_link = nsections;
	}

	add_symtab(efile);

	nsections = slist_elf_section_size(&efile->sections);
	for (i = 0; i < nsections; ++i) {
		esection = slist_elf_section_get(&efile->sections, i);
		if (esection->shdr.sh_type == SHT_SYMTAB)
			esection->shdr.sh_link = nsections;
	}

	add_strtab(efile);
	add_shstrtab(efile);
}

int
elf_convert_link(FILE *file, psyq_link *link)
{
	Elf32_Ehdr ehdr;
	elf_file efile;
	elf_section *esection;
	size_t size;
	size_t i;

	psyq_to_elf(link, &efile);

	memset(&ehdr, 0, sizeof(ehdr));

	ehdr.e_ident[EI_MAG0] = ELFMAG0;
	ehdr.e_ident[EI_MAG1] = ELFMAG1;
	ehdr.e_ident[EI_MAG2] = ELFMAG2;
	ehdr.e_ident[EI_MAG3] = ELFMAG3;

	ehdr.e_ident[EI_CLASS] = ELFCLASS32;
	ehdr.e_ident[EI_DATA] = ELFDATA2LSB;
	ehdr.e_ident[EI_VERSION] = EV_CURRENT;
	ehdr.e_ident[EI_OSABI] = ELFOSABI_NONE;

	ehdr.e_type = ET_REL;
	ehdr.e_machine = EM_MIPS;
	ehdr.e_version = EV_CURRENT;
	ehdr.e_shoff = sizeof(ehdr);
	ehdr.e_flags = EF_MIPS_NOREORDER | 0x1000;
	ehdr.e_ehsize = sizeof(ehdr);
	ehdr.e_shentsize = sizeof(Elf32_Shdr);

	ehdr.e_shnum = slist_elf_section_size(&efile.sections);
	ehdr.e_shstrndx = ehdr.e_shnum - 1;

	fwrite(&ehdr, sizeof(ehdr), 1, file);

	size = ehdr.e_shoff + (ehdr.e_shnum * sizeof(Elf32_Shdr));

	for (i = 0; i < slist_elf_section_size(&efile.sections); ++i) {
		esection = slist_elf_section_get(&efile.sections, i);
		if (i) {
			esection->shdr.sh_offset = size;
			size += esection->size;
		}
		fwrite(&esection->shdr, sizeof(esection->shdr), 1, file);
	}

	for (i = 0; i < slist_elf_section_size(&efile.sections); ++i) {
		esection = slist_elf_section_get(&efile.sections, i);
		if ((esection->data != NULL) && esection->size)
			fwrite(esection->data, esection->size, 1, file);
	}

	return ferror(file);
}

SLIST_DEF(elf_section);
SLIST_DEF(elf_sym);
